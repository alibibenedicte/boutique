/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univ.cic.boutique.resources;

import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import services.EmployeService;
import tg.univlome.cic.boutique.boutique.entites.Employe;

/**
 *
 * @author benex
 */
@Path("/employe")
public class EmployeResource {
    private EmployeService service;
   
    public EmployeResource() {
       this.service = EmployeService.getInstance();
    }

    @GET
    @Path("/list")
    public List<Object> lister() {
      return this.service.getEmployes();
    }

    @GET
    @Path("/{id}")
    public Employe trouver(@PathParam("id") Long id) {
      return (Employe) this.service.trouver(id);   
    }

    @GET
    @Path("/count")
    public int compter() {
      return this.service.compter();   
    }

    @POST
    public Employe ajouter(Employe employe) {
      return (Employe) this.service.ajouter(employe);
    }

    @PUT
    public Employe modifier(Employe employe) {
       return (Employe) this.service.modifier(employe);
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id) {
       this.service.supprimer(id);
    }
}

