/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univ.cic.boutique.resources;

import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import services.ClientService;
import tg.univlome.cic.boutique.boutique.entites.Client;

/**
 *
 * @author benex
 */
@Path("/client")
public class ClientResource {
     private ClientService service;

   public ClientResource() {
    this.service = ClientService.getInstance();
   }

    @GET
    @Path("/list")
    public List<Object> lister() {
      return this.service.getClients();
    }

    @GET
    @Path("/{id}")
    public Client trouver(@PathParam("id") Long id) {
      return (Client) this.service.trouver(id);   
    }

    @GET
    @Path("/count")
    public int compter() {
      return this.service.compter();   
    }

    @POST
    public Client ajouter(Client client) {
      return (Client) this.service.ajouter(client);
    }

    @PUT
    public Client modifier(Client client) {
       return (Client) this.service.modifier(client);
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id) {
       this.service.supprimer(id);
    }
}
