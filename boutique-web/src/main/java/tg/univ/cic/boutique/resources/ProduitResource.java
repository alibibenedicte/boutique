/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univ.cic.boutique.resources;

import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import services.ProduitService;
import tg.univlome.cic.boutique.boutique.entites.Produit;

/**
 *
 * @author benex
 */
@Path("/produitAchete")
public class ProduitResource {
    private ProduitService service;

    public ProduitResource() {
       this.service = ProduitService.getInstance();
    }    

    @GET
    public List<Object> lister() {
      return this.service.getProduits();
    }

    @GET
    @Path("/{id}")
    public Produit trouver(@PathParam("id") Long id) {
      return (Produit) this.service.trouver(id);   
    }

    @GET
    @Path("/count")
    public int compter() {
      return this.service.compter();   
    }

    @POST
    public Produit ajouter(Produit produit) {
      return (Produit) this.service.ajouter(produit);
    }

    @PUT
    public Produit modifier(Produit produit) {
       return (Produit) this.service.modifier(produit);
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id) {
      this.service.supprimer(id);
    }
}
