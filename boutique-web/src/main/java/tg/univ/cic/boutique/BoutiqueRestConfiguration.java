package tg.univ.cic.boutique;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author 
 */
@ApplicationPath("api")
public class BoutiqueRestConfiguration extends Application {
    
}
