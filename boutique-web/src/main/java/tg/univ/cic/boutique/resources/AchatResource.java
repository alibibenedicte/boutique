/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univ.cic.boutique.resources;

import java.util.List;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import services.AchatService;
import tg.univlome.cic.boutique.boutique.entites.Achat;

/**
 *
 * @author benex
 */

@Path("/achat")
public class AchatResource {
    AchatService service;

    public AchatResource() {
      this.service = AchatService.getInstance();
    }
    
    @GET
    public List<Object> lister() {
      return this.service.getAchats();
    }

    @GET
    @Path("/{id}")
    public Object trouver(@PathParam("id") Long id) {
      return this.service.trouver(id);   
    }

    @GET
    @Path("/count")
    public int compter() {
      return this.service.compter();   
    } 

    @POST
    public Object ajouter(Achat achat) {
      return this.service.ajouter(achat);
    }

    @PUT
    public Object modifier(Achat achat) {
       return this.service.modifier(achat);
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id) {
       this.service.supprimer(id);
    } 
    
    
}
