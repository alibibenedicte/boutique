
package tg.univ.cic.boutique.client.metier;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.boutique.entites.Produit;

/**
 *
 * @author benex
 */

public class ProduitClient {
    private String url;
    private static ProduitClient  INSTANCE;
    
     private ProduitClient(String url) {
         this.url= url;
        
    }

    public synchronized static ProduitClient getInstance(String url)
    {
        if (INSTANCE == null)
            INSTANCE = new ProduitClient(url);
 
        return INSTANCE;
    }
    
    public Produit trouver(Long id){
        
        Client client = ClientBuilder.newClient();
        
        Response reponse = client.target(url)
                .path(""+id+"")
                .request(MediaType.APPLICATION_JSON)
                .get();
        
        if (reponse.getStatus()==200){
            return reponse.readEntity(Produit.class);
        }
        
        throw new IllegalStateException("produit introuvable"); 
    }
    
    public void ajouter(Produit p){
     
        Client client = ClientBuilder.newClient();
        
        Response reponse = client.target(url)
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (reponse.getStatus()==200){
            System.out.println("Ajout reussie");
        }
        
        throw new IllegalStateException("Echec de l'ajout"); 
    
    }
    
   
    
}
