
package tg.univ.cic.boutique.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.boutique.entites.Produit;


/**
 *
 * @author benex
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String url = "http://localhost:8080/boutique/api/produit/trouver/";
        Client client = ClientBuilder.newClient();
        Response reponse = client.target(url)
                .path("2")
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (reponse.getStatus()==200){
            Produit produit = reponse.readEntity(Produit.class);
            System.out.println(produit);
        }else  
        {
        System.out.println(reponse.getStatus());
        }
    }
    
}
