/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.boutique.entites;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author benex
 */
public class Client extends Personne{
    private String cin;
    private String carteVisa;

    public Client() {
    }

    public Client(String cin, String carteVisa, String nom, String prenoms, LocalDate dateNaissance) {
        super(nom, prenoms, dateNaissance);
        this.cin = cin;
        this.carteVisa = carteVisa;
    }

    public Client(String cin, String carteVisa, Long id, String nom, String prenoms, LocalDate dateNaissance) {
        super(id, nom, prenoms, dateNaissance);
        this.cin = cin;
        this.carteVisa = carteVisa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client c = (Client) o;
        return cin.equals(c.cin) && carteVisa.equals(c.carteVisa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cin, carteVisa);
    }

    @Override
    public String toString() {
        return "Client{" +
                "cin='" + cin + '\'' +
                ", carteVisa='" + carteVisa + '\'' +
                '}';
    }
}
